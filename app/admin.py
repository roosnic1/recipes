from django.contrib import admin

# Register your models here.
from reversion.admin import VersionAdmin

from app.models import Ingredient


@admin.register(Ingredient)
class YourModelAdmin(VersionAdmin):
    pass