import reversion
from django.db import models
from django.utils.translation import gettext as _

# Create your models here.

GRAM = 'gm'
LITER = 'lt'
TEA_SPOON = 'ts'
EAT_SPOON = 'es'
DASH = 'ds'
NONE = 'no'

WEIGHT_UNITS = [
    (GRAM, _("weight unit gram")),
    (LITER, _("weight unit liter")),
    (TEA_SPOON, _("weight unit tea spoon")),
    (EAT_SPOON, _("weight unit eat spoon")),
    (DASH, _("weight unit dash")),
    (NONE, _("weight unit none"))
]


@reversion.register()
class Ingredient(models.Model):
    name = models.CharField(_('ingredient name'), max_length=150, blank=False, null=False)
    default_unit = models.CharField(
        _('ingredient default unit'),
        max_length=2,
        choices=WEIGHT_UNITS,
        default=GRAM,
    )
    description = models.TextField(_('ingredient description'), blank=True, null=True)
    link = models.URLField(_('ingredient url'), blank=True, null=True)
